<?php 
    wp_footer();
?>


<div class="footerSite_um">
    <div class="post_blog">
        <h1 class="textos_footer_h1">Posts mais vistos</h1>
        <?php 
            wpp_get_mostpopular([
                'limit' => '4'
            ]);
        ?>
    </div>
    <div class="post_blog">
        <h1 class="textos_footer_h1">Posts recentes</h1>
            <?php
            // Define our WP Query Parameters
            $the_query = new WP_Query( 'posts_per_page=4' ); ?>
            
            <?php
            // Start our WP Query
            while ($the_query -> have_posts()) : $the_query -> the_post();
            // Display the Post Title with Hyperlink
            ?>
            
            <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
            
            <?php
            // Display the Post Excerpt
            //the_excerpt(__('(more…)')); 
            ?>

            <?php
            // Repeat the process and reset once it hits the limit
            endwhile;
            wp_reset_postdata();
            ?>
    </div>
    <div class="link_page_footer" >
        <h1 class="textos_footer_h1">Links</h1>
        
            <a href="http://localhost:10023#scrollHere">Serviços</a>
            <a href="http://localhost:10023quem-somos/">Quem somos</a>
            <a href="http://localhost:10023blog/">Blog</a>
            <a href="#">Politicas de privacidade</a>
            <a href="#">Termos de uso</a>
       
    </div>
</div>
<div class="footerSite_dois">
    <div class="logo_cnpj">
        <?php the_custom_logo(); ?>
        <p>CNPJ: 45987454352</p>
    </div>

    <div class="cadastro_NL">
        <p>Cadastre-se em nossa newsletter!</p>
        <!--  tnp tnp-subscription -->
        <div  class="form-cadastro" >
            <form method="post" action="http://localhost:10023?na=s">
            <input type="hidden" name="nlang" value=""><div class="tnp-field tnp-field-email"><label for="tnp-1"></label>
            <input class="tnp-email" type="email" name="ne" id="tnp-1" placeholder="Digite seu email" required>
        </div>
        <div class="tnp-field tnp-privacy-field"><label><input type="checkbox" name="ny" required class="tnp-privacy">
         Aceite os termos de privacidade para continuar</label>
        </div>
        <div class="tnp-field tnp-field-button">
            <input class="tnp-submit" type="submit" value="Cadastrar" >
        </div>
        </form>
        </div>
      <!--  -->  
    </div>
        <!-- Pegar infos de rede social -->
        <?php 
        pegar_redes_sociais();

        function i(){
            global $insta;
            echo $insta;
        }
        function l(){
            global $linked;
            echo $linked;
        }
        function m(){
            global $mail;
            echo $mail;
        }
        function number(){
            global $number;
            echo $number;
        }
        function e(){
            global $ender;
            echo $ender;
        }
        ?>
    <div class="end_links">
        <p><?php e(); ?></p>
        <p><?php number(); ?></p>
        <div class="redeSocialFooter">
            <a href="<?php i(); ?>"><img src="<?php echo IMAGE_DIR . '/Instagram.svg'?>" alt=""></a>
            <a href="<?php l(); ?>"><img src="<?php echo IMAGE_DIR . '/LinkedIn.svg'?>" alt=""></a>
            <a href="<?php m(); ?>"><img src="<?php echo IMAGE_DIR . '/Email.svg'?>" alt=""></a>
        </div>
    </div>
</div>
<div class="cpr-footer">
    <p>Copyright ⒸExpand JR 2023</p>
    
</div>



</body>
</html>