<?php 
    function estilos_tema(){
        //Registro
        wp_register_style('style_Lobo_reset', STYLES_DIR . '/reset.css', [], '1.0.0', false);
        wp_register_style('style', get_stylesheet_uri(), [], '1.0.0', false);
        wp_register_style('style_Lobo_Header', STYLES_DIR . '/header.css', [], '1.0.0', false);
        wp_register_style('style_Lobo_Footer', STYLES_DIR . '/footer.css', [], '1.0.0', false);
        wp_register_style('style_quem-somos', STYLES_DIR . '/page-quem-somos.css', [], '1.0.0', false);
        wp_register_style('style_home', STYLES_DIR . '/home.css', [], '1.0.0', false);
        wp_register_style( 'style_carrosel', STYLES_DIR . '/cases-carrossel.css', [], '1.0.0', false );
        wp_register_style( 'style_contatos', STYLES_DIR . '/page-contatos-estilos.css', [], '1.0.0', false );
        wp_register_style( 'style_blog', STYLES_DIR . '/blog-estilos.css', [], '1.0.0', false );
        wp_register_style( 'style_newsletter', STYLES_DIR . '/newsletter.css', [], '1.0.0', false );
        wp_register_style( 'style_single', STYLES_DIR . '/single.css', [], '1.0.0', false );

        wp_register_script( 'js_menu_hamb', JAVA_DIR . '/menu-hamburg.js', [], '1.0.0', true );
        wp_register_script('scriptJava_cart', JAVA_DIR . '/modal.js', [], '1.0.0', true);
        wp_register_script('js_animation_home', JAVA_DIR . '/animation.js', [], '1.0.0', true);
        wp_register_script('js_pop_up', JAVA_DIR . '/popup.js', [], '1.0.0', true);
        wp_register_script('js_carousel_cases', JAVA_DIR . '/carousel-cases.js', [], '1.0.0', true);
        wp_register_script('js_carousel_apoiadores', JAVA_DIR . '/carrossel-apoiadores.js', [], '1.0.0', true);
        wp_register_script('js_jornada_cliente', JAVA_DIR . '/jornada-cliente.js', [], '1.0.0', true);
        wp_register_script('js_blog_page', JAVA_DIR . '/search-blog-page.js', [], '1.0.0', true);
        wp_register_script('js_header_service', JAVA_DIR . '/header-service.js', [], '1.0.0', true);


        //Fila
        wp_enqueue_style('style_Lobo_reset');
        wp_enqueue_style('style');
        wp_enqueue_style('style_Lobo_Header');
        wp_enqueue_style( 'style_Lobo_Footer' );
        wp_enqueue_style('style_home');
        wp_enqueue_style('style_quem-somos');
        wp_enqueue_style( 'style_carrosel' );
        wp_enqueue_style('style_contatos');
        wp_enqueue_style('style_blog');
        wp_enqueue_style('style_newsletter');
        wp_enqueue_style('style_single');

        wp_enqueue_script('js_menu_hamb');
        wp_enqueue_script('scriptJava_cart');
        wp_enqueue_script('js_animation_home');
        wp_enqueue_script('js_pop_up');
        wp_enqueue_script('js_carousel_cases');
        wp_enqueue_script('js_carousel_apoiadores');
        wp_enqueue_script('js_jornada_cliente');
        wp_enqueue_script('js_blog_page');
        wp_enqueue_script('js_header_service');

    }


?>