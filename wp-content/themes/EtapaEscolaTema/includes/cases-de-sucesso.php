<?php 

// Função que pega as imagens do grupo de apoiadores no AdvancedCustomFields
function getCases()
{
    $acf_field_group = acf_get_field_group(52);
    $acf_fields = acf_get_fields(52);
    $counter = 1;

    for ($i=0; $i <= count($acf_fields); $i++) { 
        if(($i % 3 == 0) && ($i > 0)) {
            $nome = get_field('nome-da-pessoa-' . $counter);
            $img = get_field('img-da-pessoa-' . $counter);
            $desc = get_field('descricao-da-pessoa-' . $counter);
            $size = 'full';
            
            ?>
            <div class="carousel-item" id="<?=$counter?>">
                <div class="slide-cases-sucesso">
                    <div class="txt-titulo-desc">
                        <p><?php echo $desc; ?></p>
                        <div class="img-nm">
                            <?php echo wp_get_attachment_image( $img, $size ); ?>
                            <p><span><?php echo $nome; ?></span></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
            $counter++;
        }
    }
}

// Função que desenha os Dots de acordo com a quantidade de Cases
function implementarDotsCases()
{
    $acf_fields = acf_get_fields(52);
    $numeroDeDots = count($acf_fields)/3;

    ?>
    <a class="active"></a>
    <?php 
    for ($i=0; $i < $numeroDeDots - 1; $i++) { 
        ?>
        <a></a>
        <?php 
    }
}
?>