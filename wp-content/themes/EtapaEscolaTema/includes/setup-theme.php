<?php 
    function setup_theme_pf(){
        add_theme_support( 'custom-logo' );
        add_theme_support( 'menus' );
        add_theme_support( 'widgets' );	
        add_theme_support( 'post-thumbnails' );
    }
?>