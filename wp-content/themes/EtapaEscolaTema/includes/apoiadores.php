<?php 

// Função que pega as imagens do grupo de apoiadores no AdvancedCustomFields
function getApoiadores()
{
    $acf_field_group = acf_get_field_group(78);
    $acf_fields = acf_get_fields(78);
    $counter = 0;

    foreach ($acf_fields as $field) {
        $name = $field['name'];
        $size = 'full';
        $image = get_field($name);
        
        ?>
        <div class="apoiador" id="<?=$counter?>">
            <?php echo wp_get_attachment_image( $image, $size ); ?>
        </div>
        <?php 
        $counter++;
    }
}

// Função que desenha os Dots de acordo com a quantidade de apoiadores
function implementarDots()
{
    $acf_fields = acf_get_fields(78);
    $numeroDeDots = count($acf_fields) - 3;

    ?>
    <a class="dot-apoiadores active"></a>
    <?php 
    for ($i=0; $i < $numeroDeDots - 1; $i++) { 
        ?>
        <a class="dot-apoiadores"></a>
        <?php 
    }
}
?>