<?php 
//Template name: home
get_header();
?>

<section class = "expandaseushorizontes-section">
    <div class = "div-maior">
        <h1 class = "titulo">Expanda seus horizontes</h1>
        <h2 class = "subtitulo">através das nossas soluções internacionais de alto impacto.</h2>
        <button class = "botao"><a href="http://localhost:10023/contato/">Fale com um especialista</a></button>
    </div>
</section>

<!--POPUP Servico-->
<div class="servico-pop-up-caixa">
    <div class="servico-pop-up">
        <img src="<?php echo IMAGE_DIR . '/fechar-pop-up.svg' ?>" alt="" class="botao-sair-pop-up">
        <div class="circulo-branco"></div>
    
        <h3 class="servico-pop-up-nome texto-azul"></h3>
    
        <p class="servico-descricao texto-azul">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>

        <?php $linkPaginaContatos = "http://localhost:10023/contato/"?>
        <a href=<?= $linkPaginaContatos ?>><button type="button" class="botao-diagnostico texto-azul">Diagnóstico Gratuito</button></a>
    </div>
</div>

<div class="fundo-acinzentado"></div>

<!-- SEÇÃO "NOSSOS SERVIÇOS"-->
<section id="scrollHere" class="secao-nossos-servicos">
    <h1 class="titulo-secao texto-azul">Nossos Serviços</h1>

    <div class="servicos">
        <div class="servicos-tipo" id="para-voce">
            <h1 class="titulo-secao texto-azul">Para Você</h1>

            <div class="servicos-pessoas-coluna">
                <div class="servico" id="dupla-cidadania">
                    <div class="circulo-cinza">
                        <img src="<?= IMAGE_DIR . "/passaportes-troca.png"?>" alt="" class="imagem-troca">
                    </div>
                    <h3 class="servico-nome texto-azul">Dupla Cidadania Portuguesa</h3>
                </div>

                <div class="servico" id="assesoria-passaporte">
                    <div class="circulo-cinza">
                        <img src="<?= IMAGE_DIR . "/passaporte.png"?>" alt="" class="imagem-passaporte">  
                    </div>
                    <h3 class="servico-nome texto-azul">Assessoria para Emissão de Passaporte Brasileiro</h3>
                </div>
            </div>
        </div>

        <div id="servicos-linha"></div>

        <div class="servicos-tipo" id="para-sua-empresa">
            <h1 class="titulo-secao texto-azul">Para Sua Empresa</h1>

            <div class="servicos-empresas-linha">
                <div class="servico" id="analise-burocratica">
                    <div class="circulo-cinza">
                        <img src="<?= IMAGE_DIR . "/burocracia.png"?>" alt="" class="imagem-burocracia">
                    </div>
                    <h3 class="servico-nome texto-azul">Análise Burocrática</h3>
                </div>

                <div class="servico" id="estudo-mercado">
                    <div class="circulo-cinza">
                        <img src="<?= IMAGE_DIR . "/estudo.png"?>" alt="" class="imagem-estudo">
                    </div>
                    <h3 class="servico-nome texto-azul">Estudo de Análise de Mercado</h3>
                </div>
            </div>

            <div class="servicos-empresas-linha">
                <div class="servico"  id="planejamento-logistico">
                    <div class="circulo-cinza">
                        <img src="<?= IMAGE_DIR . "/mini-guindaste.png"?>" alt="" class="imagem-guindaste">
                    </div>
                    <h3 class="servico-nome texto-azul">Planejamento Logístico</h3>
                </div>

                <div class="servico" id="prospeccao-internacional">
                    <div class="circulo-cinza">
                        <img src="<?= IMAGE_DIR . "/acordo.png"?>" alt="" class="imagem-acordo">
                    </div>
                    <h3 class="servico-nome texto-azul">Prospecção Internacional</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<!--FIM Seção: "NOSSOS SERVIÇOS"-->


<!-- SEÇÃO "RESULTADOS"-->
<section class="secao-resultados">
    <h1 class="titulo-secao texto-azul" id="titulo-resultados">Resultados</h1>

    <div class="resultados">
        <div class="resultado">
            <div class="circulo-cinza"><img src="<?= IMAGE_DIR . "/calendario.png"?>" alt=""></div>
            <h1 class="resultado-numero texto-azul" id="resultado-mercado">6</h1>
            <h2 class="texto-azul resultado-nome">Anos no Mercado</h2>
        </div>

        <div class="resultado">
            <div class="circulo-cinza"><img src="<?= IMAGE_DIR . "/apresentacao.png"?>" alt=""></div>
            <h1 class="resultado-numero texto-azul" id="resultado-projetos">+70</h1>
            <h2 class="texto-azul resultado-nome">Projetos Realizados</h2>
        </div>

        <div class="resultado">
            <div class="circulo-cinza"><img src="<?= IMAGE_DIR . "/graduado.png"?>" alt=""></div>
            <h1 class="resultado-numero texto-azul" id="resultado-estudantes">+100</h1>
            <h2 class="texto-azul resultado-nome">Estudantes Impactados</h2>
        </div>

        <div class="resultado">
            <div class="circulo-cinza"><img src="<?= IMAGE_DIR . "/review.png"?>" alt=""></div>
            <h1 class="resultado-numero texto-azul" id="resultado-satisfacao">+80%</h1>
            <h2 class="texto-azul resultado-nome">Satisfação dos Clientes</h2>
        </div>

    </div>
</section>
<!--FIM Seção: "RESULTADOS"-->

<section class="secao-cases-sucesso"> 
    <h1 class="titulo-secao texto-azul" >Cases de sucesso</h1>
    <div class="carousel">
        <div class="carousel-inner">
            <?php getCases() ?>
        </div>
        
        <img class="carousel-prev" width=28 heigth=45 src="<?php echo IMAGE_DIR . '/seta-esq.svg'; ?>"  alt="seta">
        <img class="carousel-next"  width=28 heigth=45 src="<?php echo IMAGE_DIR . '/seta-dir.svg'; ?>"   alt="seta">
    
    </div>
    <div class="carousel-dots">
        <?php implementarDotsCases() ?>
    </div>
</section>

<!-- SEÇÃO "APOIADORES"-->
<section class="secao-apoiadores">
    <h1 class="titulo-secao texto-azul">Apoiadores</h1>
    <div class="carrossel-apoiadores">
        <div class="apoiadores">
            <?php getApoiadores() ?>
        </div>
        <a class="anterior-apoiador"><img src="<?= IMAGE_DIR . "/seta-esq.svg"?>" alt="" class="imagens-setas"></a>
        <a class="proximo-apoiador"><img src="<?= IMAGE_DIR . "/seta-dir.svg"?>" alt="" class="imagens-setas"></a>
    </div>
    <div class="carrossel-dots">
        <?php implementarDots() ?>
    </div>
</section>
<!-- FIM Seção: "Apoiadores" -->



<?php

get_footer();
?>