function menu() {
    let interiorMenuHamb = document.querySelector(".interior-menu-hamb");

    let me = document.querySelector(".menus-hamb");
    me.classList.toggle("change");
    if(op == 2) {
        op = 1;
        interiorMenuHamb.classList.remove("fade-in");
        interiorMenuHamb.classList.add("fade-out");
        meuIntervalo = setInterval(menuNone, 2000);
    } else {
        interiorMenuHamb.classList.remove("fade-out");
        interiorMenuHamb.classList.add("fade-in");
        interiorMenuHamb.style.display = "block";
        op++;
    }
    
}

function menuNone() {
    let interiorMenuHamb = document.querySelector(".interior-menu-hamb");
    interiorMenuHamb.style.display = 'block';
    clearInterval(meuIntervalo);
}

function fecharModal() {
    let me = document.querySelector(".menus-hamb");
    me.classList.remove("change");
    let interiorMenuHamb = document.querySelector(".interior-menu-hamb");
    interiorMenuHamb.style.display = "none";
    op = 1;
}

function ptmudatdr() {
    let pt = document.querySelector("#pt-en-tdr");
    pt.style.display = "none";
    let en = document.querySelector("#en-pt-tdr");
    en.style.display = "block";
}

function enmudatdr() {
    let en = document.querySelector("#en-pt-tdr");
    en.style.display = "none";
    let pt = document.querySelector("#pt-en-tdr");
    pt.style.display = "block";
}

var op = 1;
var meuIntervalo;

document.querySelector(".menus-hamb").addEventListener("click", menu); 
document.querySelector("#pt-en-tdr").addEventListener("click", ptmudatdr);
document.querySelector("#en-pt-tdr").addEventListener("click", enmudatdr);

window.addEventListener('resize', (event) => {
    var viewport_width = window.innerWidth;
    
    if(viewport_width >= 787) {
        fecharModal();
    }
})