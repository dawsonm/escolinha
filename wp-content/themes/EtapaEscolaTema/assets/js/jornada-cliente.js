// Função que muda o texto mostrado
function mudarTexto() {
    texto.innerHTML = listaTextos[elipseAtivaIndex];
}


// Função que deixa a elipse azul de acordo o "elipseAtivaIndex"
function ativarElipse() {
    for (let i = 0; i < listaElipses.length; i++) {
        if(i > elipseAtivaIndex) {
            listaElipses[i].classList.remove("etapa-ativa");
        } else {
            listaElipses[i].classList.add("etapa-ativa");
        }
    } 

    // Deixar o caminho de linhas azul até a elipse da etapa
    for (let j = 0; j < listaLinhas.length; j++) {
        if(j < elipseAtivaIndex) {
            listaLinhas[j].classList.add("navegador-linha-ativa");
        } else {
            listaLinhas[j].classList.remove("navegador-linha-ativa");
        }
    }
}


var listaElipses = document.querySelectorAll(".etapa-elipse-menor");
var listaTitulos = document.querySelectorAll(".etapa-titulo");
var listaLinhas = document.querySelectorAll(".navegador-linha");

var texto = document.querySelector(".etapa-texto");
var listaTextos = Array();

var elipseAtivaIndex = 0;

const TextoContato = "Primeiro Contato semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque. In dapibus nulla mi, eu elementum quam consequat ac. Nulla mattis, nunc eget viverra imperdiet, justo odio vehicula diam, a scelerisque odio enim id felis. Phasellus tempor libero nec posuere ultrice <br> <br> Vivamus sit amet fermentum eros. Praesent eget vehicula dolor. Morbi sem magna, scelerisque eu est non, rutrum facilisis massa. Cras ac nulla felis. Nunc eu eleifend nulla. Donec vel ornare diam.";
listaTextos.push(TextoContato);

const TextoDiagnostico = "Diagnóstico semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque. In dapibus nulla mi, eu elementum quam consequat ac. Nulla mattis, nunc eget viverra imperdiet, justo odio vehicula diam, a scelerisque odio enim id felis. Phasellus tempor libero nec posuere ultrice <br> <br> Vivamus sit amet fermentum eros. Praesent eget vehicula dolor. Morbi sem magna, scelerisque eu est non, rutrum facilisis massa. Cras ac nulla felis. Nunc eu eleifend nulla. Donec vel ornare diam.";
listaTextos.push(TextoDiagnostico);

const TextoNegociacao = "Negociação semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque. In dapibus nulla mi, eu elementum quam consequat ac. Nulla mattis, nunc eget viverra imperdiet, justo odio vehicula diam, a scelerisque odio enim id felis. Phasellus tempor libero nec posuere ultrice <br> <br> Vivamus sit amet fermentum eros. Praesent eget vehicula dolor. Morbi sem magna, scelerisque eu est non, rutrum facilisis massa. Cras ac nulla felis. Nunc eu eleifend nulla. Donec vel ornare diam.";
listaTextos.push(TextoNegociacao);

const TextoInicio = "Inicio do Projeto semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque. In dapibus nulla mi, eu elementum quam consequat ac. Nulla mattis, nunc eget viverra imperdiet, justo odio vehicula diam, a scelerisque odio enim id felis. Phasellus tempor libero nec posuere ultrice <br> <br> Vivamus sit amet fermentum eros. Praesent eget vehicula dolor. Morbi sem magna, scelerisque eu est non, rutrum facilisis massa. Cras ac nulla felis. Nunc eu eleifend nulla. Donec vel ornare diam.";
listaTextos.push(TextoInicio);

// Adiciona o evento do click em cada elipse menor
Array.from(listaElipses).forEach(elipse => {
    elipse.addEventListener('click', event => {
        elipseAtivaIndex = Array.from(listaElipses).indexOf(event.target);
        ativarElipse();
        mudarTexto();
    })
})

// Adiciona o evento do click no titulo de cada etapa
Array.from(listaTitulos).forEach(titulo => {
    titulo.addEventListener('click', event => {
        elipseAtivaIndex = Array.from(listaTitulos).indexOf(event.target);
        ativarElipse();
        mudarTexto();
    })
})

ativarElipse();
mudarTexto();