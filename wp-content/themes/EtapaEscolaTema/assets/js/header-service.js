var service = document.getElementById("menu-item-23");

if(window.location.href.indexOf("http://localhost:10023") == true){
    const aElement = service.querySelector('li a');
    aElement.href = '';
    const targetDiv = document.getElementById('scrollHere');
    service.addEventListener('click', function() {
    targetDiv.scrollIntoView({ behavior: 'smooth' });
    });

}
else{
    const aElement = service.querySelector('li a');
    aElement.href = 'http://localhost:10023#scrollHere';
}