/**Função que anima os "obj"**/
function animateValue(obj, start, end, duration, string, positionString) {
    let startTimestamp = null;
    const step = (timestamp) => {
        if (!startTimestamp) startTimestamp = timestamp;
        const progress = Math.min((timestamp - startTimestamp) / duration, 1);
        if(positionString == "Antes") {
            obj.innerHTML = `${string}${Math.floor(progress * (end - start) + start)}`;
        }
        else if(positionString == "Depois") {
            obj.innerHTML = `${Math.floor(progress * (end - start) + start)}${string}`;
        }
        else {
            obj.innerHTML = `${Math.floor(progress * (end - start) + start)}`;
        }
        if (progress < 1) {
            window.requestAnimationFrame(step);
        }
    };
    window.requestAnimationFrame(step);
  }
  
  /**Função que verifica se o "elem" entrou na tela**/
  function isScrolledIntoView(elem)
  {
    jQuery( function($) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
    
        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();
        
        let verificador = (elemBottom <= docViewBottom) && (elemTop >= docViewTop);
        
        if(verificador) {
            entrouNaTela = true;
            
            animateValue(resultadoMercado, 0, 6, 3000, "", "");
            animateValue(resultadoProjetos, 0, 70, 3000, "+", "Antes");
            animateValue(resultadoEstudantes, 0, 100, 3000, "+", "Antes");
            animateValue(resultadoSatisfacao, 0, 80, 3000, "%", "Depois");
        }
    })
  } 
  
  
  /**Pegando os elemento que vão ser animados**/
  var resultadoMercado = document.querySelector('#resultado-mercado');
  var resultadoProjetos = document.querySelector('#resultado-projetos');
  var resultadoEstudantes = document.querySelector('#resultado-estudantes');
  var resultadoSatisfacao = document.querySelector('#resultado-satisfacao');
  
  /**Pegando o título da seção, pois quando ele entrar na tela, a animação acontece**/
  let tituloResultados = document.querySelector("#titulo-resultados");
  
  var entrouNaTela = false;
  
  /**Verficando se houve scroll na tela**/
  window.addEventListener('scroll', () => {
    if(entrouNaTela == false) {
        isScrolledIntoView(tituloResultados);
    }
  });