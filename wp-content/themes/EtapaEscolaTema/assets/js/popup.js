function mostrar_pop_up() {
    let fundo = document.querySelector(".fundo-acinzentado");
    fundo.style.display = "block";

    let pop_up = document.querySelector(".servico-pop-up-caixa");
    pop_up.style.display = "flex";
}

function abrir_dupla_cidadania() 
{
    let nome = document.querySelector(".servico-pop-up-nome");
    let circulo = document.querySelector(".circulo-branco");
    let imagem = document.createElement("img");
    imagem.src = "/wp-content/themes/EtapaEscolaTema/assets/images/passaportes-troca.png"
    nome.innerText = "Dupla Cidadania Portuguesa";
    circulo.append(imagem);
    mostrar_pop_up();
}

function abrir_assesoria_passaporte() 
{
    let nome = document.querySelector(".servico-pop-up-nome");
    let circulo = document.querySelector(".circulo-branco");
    let imagem = document.createElement("img");
    imagem.src = "/wp-content/themes/EtapaEscolaTema/assets/images/passaporte.png"
    nome.innerText = "Assessoria para Emissão de Passaporte Brasileiro";
    circulo.append(imagem);
    mostrar_pop_up();
}

function abrir_analise_burocratica() 
{
    let nome = document.querySelector(".servico-pop-up-nome");
    let circulo = document.querySelector(".circulo-branco");
    let imagem = document.createElement("img");
    imagem.src = "/wp-content/themes/EtapaEscolaTema/assets/images/burocracia.png"
    nome.innerText = "Análise Burocrática";
    circulo.append(imagem);
    mostrar_pop_up();
}

function abrir_estudo_mercado() 
{
    let nome = document.querySelector(".servico-pop-up-nome");
    let circulo = document.querySelector(".circulo-branco");
    let imagem = document.createElement("img");
    imagem.src = "/wp-content/themes/EtapaEscolaTema/assets/images/estudo.png"
    nome.innerText = "Estudo de Análise de Mercado";
    circulo.append(imagem);
    mostrar_pop_up();
}

function abrir_planejamento_logistico() 
{
    let nome = document.querySelector(".servico-pop-up-nome");
    let circulo = document.querySelector(".circulo-branco");
    let imagem = document.createElement("img");
    imagem.src = "/wp-content/themes/EtapaEscolaTema/assets/images/mini-guindaste.png"
    nome.innerText = "Planejamento Logístico";
    circulo.append(imagem);
    mostrar_pop_up();
}

function abrir_prospeccao_internacional() 
{
    let nome = document.querySelector(".servico-pop-up-nome");
    let circulo = document.querySelector(".circulo-branco");
    let imagem = document.createElement("img");
    imagem.src = "/wp-content/themes/EtapaEscolaTema/assets/images/acordo.png"
    nome.innerText = "Prospecção Internacional";
    circulo.append(imagem);
    mostrar_pop_up();
}

function fechar_pop_up()
{
    let pop_up = document.querySelector(".servico-pop-up-caixa");
    let fundo_acinzentado = document.querySelector(".fundo-acinzentado");
    pop_up.style.display = "none";
    fundo_acinzentado.style.display = "none";
    const circulo = document.querySelector(".circulo-branco")
    circulo.removeChild(circulo.firstElementChild);
}

document.querySelector(".botao-sair-pop-up").addEventListener('click', fechar_pop_up);

document.querySelector("#dupla-cidadania").addEventListener('click', abrir_dupla_cidadania);
document.querySelector("#assesoria-passaporte").addEventListener('click', abrir_assesoria_passaporte);
document.querySelector("#analise-burocratica").addEventListener('click', abrir_analise_burocratica);
document.querySelector("#estudo-mercado").addEventListener('click', abrir_estudo_mercado);
document.querySelector("#planejamento-logistico").addEventListener('click', abrir_planejamento_logistico);
document.querySelector("#prospeccao-internacional").addEventListener('click', abrir_prospeccao_internacional);