// Get the elements of the carousel
const carousel = document.querySelector('.carousel');
const carouselInner = carousel.querySelector('.carousel-inner');
const carouselItems = carousel.querySelectorAll('.carousel-item');
const carouselPrev = carousel.querySelector('.carousel-prev');
const carouselNext = carousel.querySelector('.carousel-next');
const carouselDots = document.querySelector('.carousel-dots');

// Set the initial slide index to 0 and the slide width to the width of the carousel container
let slideIndex = 0;
// slideWidthCase = carousel.offsetWidth;

// Definir a distância que o carrossel move ao avançar ou voltar
if(carouselItems.length > 0) {
    var windowWidthCases = window.innerWidth;
    if(windowWidthCases > 630) {
        var slideWidthCase = (windowWidthCases * 0.905) + (windowWidthCases * 0.05);
    } 
    // else if(windowWidthCases > 430) {
    //     var slideWidthCase = (windowWidthCases * 0.993);
    // }
    // else if(windowWidthCases <= 430) {
    //     var slideWidthCase = (windowWidthCases * 0.996);
    // }
    console.log(windowWidthCases);
}

window.addEventListener("resize", () => {
    windowWidthCases = window.innerWidth;
    slideWidthCase = (windowWidthCases * 0.905) + (windowWidthCases * 0.05);
    // if(windowWidthCases > 630) {
    //     slideWidthCase = (windowWidthCases * 0.99);
    // } 
    // else if(windowWidthCases > 430) {
    //     slideWidthCase = (windowWidthCases * 0.993);
    // }
    // else if(windowWidthCases > 347) {
    //     slideWidthCase = (windowWidthCases * 0.996);
    // }
    // else if(windowWidthCases <= 347) {
    //     slideWidthCase = (windowWidthCases * 0.996);
    // }
    console.log(windowWidthCases);
})

// Add event listeners to the navigation arrows
carouselPrev.addEventListener('click', () => {
    slideIndex--;
    updateCarousel();
});

carouselNext.addEventListener('click', () => {
    slideIndex++;
    updateCarousel();
});

// Add event listener to the dot indicators
carouselDots.addEventListener('click', event => {
if (event.target.tagName === 'A') {
    slideIndex = Array.from(carouselDots.children).indexOf(event.target);
    updateCarousel();
}
});

// Update the carousel to display the current slide
function updateCarousel() {
// Wrap the slide index if it goes beyond the first or last slide
if (slideIndex < 0) {
    slideIndex = carouselItems.length - 1;
} else if (slideIndex >= carouselItems.length) {
    slideIndex = 0;
}

// Update the position of the carousel inner container to show the current slide
carouselInner.style.transform = `translateX(-${slideIndex * slideWidthCase}px)`;

// Update the active dot indicator
Array.from(carouselDots.children).forEach(dot => {
    dot.classList.toggle('active', dot === carouselDots.children[slideIndex]);
});
}

// window.addEventListener('resize', () => {
//     slideWidth = carousel.offsetWidth;
//     updateCarousel();
// });

// Set the active dot indicator and initial slide position
updateCarousel();