// Get the elements of the carrossel
const carrossel = document.querySelector('.carrossel-apoiadores');
const carrosselInner = carrossel.querySelector('.apoiadores');
const carrosselItems = carrossel.querySelectorAll('.apoiador');
const carrosselPrev = carrossel.querySelector('.anterior-apoiador');
const carrosselNext = carrossel.querySelector('.proximo-apoiador');
const carrosselDots = document.querySelector('.carrossel-dots');

// Set the initial slide index to 0 and the slide width to the width of the carrossel container
let slideIndexApoiador = 0;

// Definir a distância que o carrossel move ao avançar ou voltar
if(carrosselItems.length > 0) {
    var windowWidth = window.innerWidth;
    if(windowWidth > 925) {
        var slideWidthApoiador = (windowWidth/100 * 17) + (windowWidth/100 * 7.3);
    }
    else if(windowWidth > 705) {
        var slideWidthApoiador = (windowWidth/100 * 28) + (windowWidth/100 * 3);
    }
    else if(windowWidth > 435) {
        var slideWidthApoiador = (windowWidth/100 * 38) + (windowWidth/100 * 12);
    } 
    else {
        var slideWidthApoiador = (windowWidth/100 * 90) + (windowWidth/100 * 90);
    }
}

window.addEventListener("resize", () => {
    if(carrosselItems.length > 0) {
        windowWidth = window.innerWidth;
        if(windowWidth > 925) {
            slideWidthApoiador = (windowWidth/100 * 17) + (windowWidth/100 * 7.3);
        }
        else if(windowWidth > 705) {
            slideWidthApoiador = (windowWidth/100 * 28) + (windowWidth/100 * 3);
        }
        else if(windowWidth > 435) {
            slideWidthApoiador = (windowWidth/100 * 38) + (windowWidth/100 * 12);
        } 
        else {
            slideWidthApoiador = (windowWidth/100 * 90) + (windowWidth/100 * 90);
        }
    }
})

// Add event listeners to the navigation arrows
carrosselPrev.addEventListener('click', () => {
    slideIndexApoiador--;
    atualizarCarrossel();
});

carrosselNext.addEventListener('click', () => {
    slideIndexApoiador++;
    atualizarCarrossel();
});

// Add event listener to the dot indicators
carrosselDots.addEventListener('click', event => {
    if (event.target.tagName === 'A') {
        slideIndexApoiador = Array.from(carrosselDots.children).indexOf(event.target);
        atualizarCarrossel();
    }
});

// Update the carrossel to display the current slide
function atualizarCarrossel() {
// Wrap the slide index if it goes beyond the first or last slide
if (slideIndexApoiador < 0) {
    slideIndexApoiador = carrosselItems.length - 4;
} else if (slideIndexApoiador >= carrosselItems.length - 3) {
    slideIndexApoiador = 0;
}

// Update the position of the carrossel inner container to show the current slide
carrosselInner.style.transform = `translateX(-${slideIndexApoiador * slideWidthApoiador}px)`;

// Update the active dot indicator
Array.from(carrosselDots.children).forEach(dot => {
    dot.classList.toggle('active', dot === carrosselDots.children[slideIndexApoiador]);
});
}

// Set the active dot indicator and initial slide position
atualizarCarrossel();