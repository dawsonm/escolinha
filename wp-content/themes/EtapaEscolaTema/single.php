<?php get_header(); ?>
<main class="single-main">
    <div class="conter-single-blog">
        <div class="content-blog-page">
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
            $img_do_post = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
            ?>
            <div class="img-titulo-e-dataDePostagem">
                <img src="<?php echo $img_do_post[0];  ?> " alt="Imagem do post">
                <div class="above-img-titulo-postagem-e-tags">
                    <h1><?php the_title(); ?></h1>
                    <div class="data-e-tag">
                        <p>
                            <?php
                                $post_date = get_the_time('j F, Y - g:i a');
                                echo 'Postado em: ' . $post_date;
                            ?>
                        </p>
                         <span id="tags-blog">
                            <?php
                                echo '<p>Tags: </p>';
                                $categories = get_the_category();
                                if (!empty($categories)) {
                                foreach ($categories as $category) {
                                    echo '<a href="'  . esc_url(get_category_link($category)) . '" >' . esc_html($category->name) . '</a> ';
                                }
                                }
                            ?>
                         </span>
                    </div>
                    <p><?php echo get_the_author(); ?></p>
                </div>
            </div>
            <div class="conteudo-blog">
                <?php the_content(); ?>
            </div>
            <?php
            endwhile; endif;
            ?>
        </div>
    
        <div class="info-adicional">
            <aside>
                <div class="posts-recentes-blog-single">
                    <h1>Posts Recentes</h1>
                    <span class="posts-recentes">
                        <?php
                            // Define our WP Query Parameters
                            $the_query = new WP_Query( 'posts_per_page=6' ); ?>
                            <?php
                            // Start our WP Query
                            while ($the_query -> have_posts()) : $the_query -> the_post();
                            // Display the Post Title with Hyperlink
                            ?>
                            <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            <?php
                            // Display the Post Excerpt
                            //the_excerpt(__('(more…)'));
                            ?>
                            <?php
                            // Repeat the process and reset once it hits the limit
                            endwhile;
                            wp_reset_postdata();
                        ?>
                    </span>
                </div>
                <div class="categoria-blog-single">
                    <h1>Categorias</h1>
                    <span div="categoria-e-imagem-blog-single">
                        <?php wp_nav_menu([
                            'menu' => 'categorias dos posts',
                            'after' => '<img src="http://localhost:10029/wp-content/uploads/2023/03/pastinha.png" alt="">',
                            ]);
                        ?>
                    </span>
                </div>
            </aside>
        </div>
    </div>
</main>
<?php get_footer(); ?>