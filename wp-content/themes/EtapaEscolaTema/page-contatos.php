<?php 
//Template name: contatos
?>
<?php get_header(); ?>

<!-- Seção: "Título da Página" -->
<section class="contatos-cabecalho">
    <div class="contatos-titulo-subtitulo">
        <h1 class="contatos-titulo">CONTATOS</h1>
        <p class="contatos-subtitulo">Fale conosco e peça seu diagnóstico gratuito!</p>
    </div>
</section>
<!-- FIM: Seção "Título da Página" -->

<!-- Seção: "Jornada do Cliente" -->
<section class="secao-jornada-cliente">
    <h2 class="secao-titulo jornada-cliente-titulo">Nossa Jornada do Cliente</h2>
    <div class="navegador">
       
        
        
        <div class= "navegador-dupla-etapa">
            <div class="navegador-etapa">
                <div class="navegador-linha" id="navegador-linha-1"></div>
                <h3 class="etapa-titulo">Primeiro Contato</h3>
                <div class="etapa-elipse-maior" id="primeiro-contato">
                    <div class="etapa-elipse-menor etapa-ativa"></div>
                </div>
            </div>
            <div class="navegador-etapa">
                <div class="navegador-linha" id="navegador-linha-2"></div>
                <h3 class="etapa-titulo texto-azul">Diagnóstico</h3>
                <div class="etapa-elipse-maior" id="diagnostico">
                    <div class="etapa-elipse-menor"></div>
                </div>
            </div>
        </div>
        <div class="navegador-dupla-etapa">
            <div class="navegador-etapa">
                <div class="navegador-linha" id="navegador-linha-3"></div>
                <h3 class="etapa-titulo texto-azul">Negociação</h3>
                <div class="etapa-elipse-maior" id="negociacao">
                    <div class="etapa-elipse-menor"></div>
                </div>
            </div>
            <div class="navegador-etapa">
                <h3 class="etapa-titulo texto-azul">Início do Projeto</h3>
                <div class="etapa-elipse-maior"  id="inicio-do-projeto">
                    <div class="etapa-elipse-menor"></div>
                </div>
            </div>
        </div>
    </div>
    <p class="etapa-texto">
        Sed semper commodo purus sit amet facilisis. Maecenas rutrum lorem lectus, ac consequat quam aliquet pellentesque. In dapibus nulla mi, eu elementum quam consequat ac. Nulla mattis, nunc eget viverra imperdiet, justo odio vehicula diam, a scelerisque odio enim id felis. Phasellus tempor libero nec posuere ultrices. 
        <br>
        <br>
        Vivamus sit amet fermentum eros. Praesent eget vehicula dolor. Morbi sem magna, scelerisque eu est non, rutrum facilisis massa. Cras ac nulla felis. Nunc eu eleifend nulla. Donec vel ornare diam.
    </p>
</section>
<!-- FIM: Seção "Jornada do Cliente" -->

<!-- Seção: "Informações" -->
<div class= "conterContato">
        <div class="conterInformação">
            <h1>Informações de Contato</h1>
            <div class="infosContato">
                <div id="telefone-contato">
                    <img src="<?php echo IMAGE_DIR . '/phone.svg'; ?>" alt="telefone">
                    <p><?php echo get_field('contato_numero');  ?></p>
                </div>
                <div id="email-contato">
                    <img src="<?php echo IMAGE_DIR . '/emailfig.svg'; ?>" alt="email">
                    <p><?php echo get_field('contato_email');  ?></p>
                </div>
                <div id="endereço-contato">
                    <img src="<?php echo IMAGE_DIR . '/placeholdermaps.svg'; ?>" alt="endereço">
                    <P><?php echo get_field('contato_endereco');  ?></P>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.2306364049073!2d-43.133855385034394!3d-22.904862485012824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817dce2f93eb%3A0x9e97773b91b93bba!2sUniversidade%20Federal%20Fluminense%20-%20Campus%20Praia%20Vermelha!5e0!3m2!1spt-BR!2sbr!4v1678208849437!5m2!1spt-BR!2sbr" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
        <div class="formulario">
            <h1 id="titulo_do_form">Formulario de Contato</h1>
            <?php echo do_shortcode('[contact-form-7 id="120" title="Formularios contatos"] ');?>
        </div>
    </div>
<!-- FIM: Seção "Informações" -->
    
<?php get_footer(); ?>