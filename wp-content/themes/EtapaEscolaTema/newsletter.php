<?php 
//Template name: newsletter
?>
<?php get_header(); ?>

<section id="secao-newsletter-confirmacao">
    <h1 id="newsletter-confirmacao-mensagem">Seu Cadastro foi concluído!</h1>

    <button id="newsletter-confirmacao-botao"><a href="http://localhost:10023">Voltar para página principal</a></button>
</section>

<?php get_footer(); ?>
