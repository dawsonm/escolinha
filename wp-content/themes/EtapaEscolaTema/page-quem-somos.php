<?php 
//Template name: quem-somos

get_header();
?>

<section class="quem-somos-secao-bem-vindo">
    <div class="quem-somos-titulo-subtitulo">
        <h1><?php the_field('titulo-page-home'); ?></h1>
        <p><?php the_field('subtitulo-page-conheca'); ?></p>
    </div>
</section>

<section class="secao-historia-da-empresa">
    <div class="como-tudo-comecou">

        <?php if( get_field('imagem_principal') ): ?>
            <img src="<?php the_field('imagem_principal'); ?>">
        <?php endif; ?>

        <div class="como-tudo-comecou-titulo-texto">
                <h2 class="como-tudo-comecou-titulo texto-azul">
                    <?php the_field('titulo_imagem_principal'); ?>
                </h2>
                <p class="como-tudo-comecou-texto"><?php the_field('texto_imagem_principal'); ?></p>
        </div>
    </div>

    <div class="mais-sobre-e-premiacoes"> 
        <div class="mais-sobre">
            <?php if( get_field('imagem_esquerda_wp') ): ?>
                <img  src="<?php the_field('imagem_esquerda_wp'); ?>">
            <?php endif; ?>

            <p><?php the_field('texto_esquerda_wp'); ?></p>
        </div>
        <div class="premiacoes">
            <?php if( get_field('imagem_direita_wp') ): ?>
                <img  src="<?php the_field('imagem_direita_wp'); ?>">
            <?php endif; ?>

            <p><?php the_field('texto_direita_wp'); ?></p>

        </div>
    </div>
</section>

<section class= "secao-o-que-nos-move">
    <div class = "o-que-nos-move-titulo texto-azul"><?php the_field('o_que_nos_move_titulo');?></div>
    <div class= "o-que-nos-move-conteudo">
        <div class = "o-que-nos-move-valores">
            <div  class="o-que-nos-move-valores-titulos texto-azul"><?php the_field('titulo_missao');?></div>
            <?php if( get_field('imagem_missao') ): ?>
                <img class="o-que-nos-move-valores-imagens" src="<?php the_field('imagem_missao'); ?>">
            <?php endif; ?>
            <p class = "o-que-nos-move-valores-paragrafos"><?php the_field('texto_missao');?></p>
        </div>
        <div class = "o-que-nos-move-valores">
            <div  class="o-que-nos-move-valores-titulos texto-azul"><?php the_field('titulo_visao');?></div>
            <?php if( get_field('imagem_visao') ): ?>
                <img class="o-que-nos-move-valores-imagens" src="<?php the_field('imagem_visao'); ?>">
            <?php endif; ?>            
            <p class = "o-que-nos-move-valores-paragrafos"><?php the_field('texto_visao');?></p>
        </div>
        <div class = "o-que-nos-move-valores">
            <div  class="o-que-nos-move-valores-titulos texto-azul"><?php the_field('titulo_valores');?></div>
            <?php if( get_field('imagem_valores') ): ?>
                <img class="o-que-nos-move-valores-imagens" src="<?php the_field('imagem_valores'); ?>">
            <?php endif; ?>            
            <p class = "o-que-nos-move-valores-paragrafos"><?php the_field('texto_valores');?></p>
        </div>    
    </div>
</section>

<section class="secao-movimento-empresa-jr">
    
        <div class = "movimento-empresa-jr-texto">
            <div class = "movimento-empresa-jr-titulo texto-azul">
                <p><?php the_field('titulo_locais');?></p>
            </div>
            <div class = "movimento-empresa-jr-paragrafo">
                <p><?php the_field('texto_locais');?></p>
            </div>

        </div>

        <?php if( get_field('imagem_locais') ): ?>
            <img  src="<?php the_field('imagem_locais'); ?>">
        <?php endif; ?>

</section>

<?php

get_footer();
?>
<section></section>

<?php

get_footer();
?>