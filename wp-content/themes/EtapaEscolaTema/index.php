
<?php 
get_header(); ?>
<!-- Inicio do Blog -->
    
<main>
<section class="contatos-cabecalho">
    <div class="contatos-titulo-subtitulo">
        <h1 class="contatos-titulo">NOSSO BLOG</h1>
        <p class="contatos-subtitulo">Nos conectamos para conectar o mundo</p>
    </div>
</section>
    <div class="conterPageBlog">
        <div class="InfoBlog">
            <?php
                // Start our WP Query
                while (have_posts()) :  the_post();
                // Display the Post Title with Hyperlink
                $img_do_post = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
            ?>
            <div class="posts">
                <img src="<?php echo $img_do_post[0];  ?> " alt="Imagem do post">
                <div class="title_cate_butn_post">
                    <h1><?php the_title(); ?></h1>
                    <div class="categoria-do-post">
                        <?php
                            $categories = get_the_category();
                            if (!empty($categories)) {
                            foreach ($categories as $category) {
                                echo '<a href="' . esc_url(get_category_link($category)) . '" >' . esc_html($category->name) . '</a> ';
                            }
                            }
                        ?>
                    </div>
                    <p> <?php echo wp_trim_words(get_the_content(), 59); ?> </p>
                    <button><a href=" <?php the_permalink() ?> ">Leia mais</a></button>
                </div>
            </div>
            <?php
                // Repeat the process and reset once it hits the limit
                endwhile;
                wp_reset_postdata();
            ?>
            <aside class="search-e-categorias">
                    <div class="barra-de-pesquisa">
                        <?php get_search_form();?>
                        <img src="<?= IMAGE_DIR . '/lupa.svg'?>" alt="">
                    </div>
                    <div class="menu-cat_post">
                        <h2>Categorias</h2>
                        <div class="categoria-e-imagem">
                        <?php wp_nav_menu([
                            'menu' => 'categorias dos posts',
                            'after' => '<img src="http://etapa-escola-03.local/wp-content/uploads/2023/03/pastinha.png" alt="">',
                            ]); 
                        ?>
                        </div>
                    </div>
            </aside>
        </div>
        <div class="paginação-blog">
                <?php the_posts_pagination(array(
            'prev_text' => '<',
            'next_text' => '>',
            )); ?>  
        </div>
    </div>    
</main>

<?php get_footer(); ?>